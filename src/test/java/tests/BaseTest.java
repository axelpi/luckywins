package tests;

import adapters.UserAdapter;
import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.WebDriverRunner;
import com.codeborne.selenide.logevents.SelenideLogger;
import io.qameta.allure.selenide.AllureSelenide;
import models.Response;
import models.UserResponseData;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import pages.BasePage;
import pages.CasinoGamesPage;
import pages.MainPage;
import utils.PropertyReader;
import wrappers.LoginForm;

public class BaseTest {

    MainPage mainPage = new MainPage();
    private final BasePage basePage = new BasePage();
    LoginForm loginForm = basePage;
    CasinoGamesPage casinoGamesPage = new CasinoGamesPage();
    UserAdapter userAdapter = new UserAdapter();

    @Parameters({"browser"})
    @BeforeMethod
    public void setup(@Optional("chrome") String browser) {
        Configuration.timeout = 10000;
        Configuration.browser = browser;
        Configuration.baseUrl = PropertyReader.getProperty("lucky.wins.url");
        Configuration.browserSize = "1920x1080";
        Configuration.headless = true;
        SelenideLogger.addListener("AllureSelenide", new AllureSelenide());
    }

    @AfterMethod
    public void closeDriver() {
        WebDriverRunner.closeWebDriver();
    }
}
