package tests;

import io.qameta.allure.LabelAnnotation;
import io.qameta.allure.Stories;

import java.lang.annotation.*;

@Documented
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD, ElementType.TYPE})
@LabelAnnotation(
        name = "tag"
)
public @interface Tag {
    String value();
}
