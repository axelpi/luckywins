package tests;

import io.qameta.allure.Feature;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;
import models.NewUser;
import models.NewUserFactory;
import org.testng.annotations.Test;
import utils.PropertyReader;

public class ApiTest extends BaseTest {

    @Test(description = "Register new user")
    @Feature("auth")
    @Story("Check that user can register")
    @Tags({"api", "regress"})
    @Severity(SeverityLevel.MINOR)
    public void registerNewUserTest() {
        NewUser user = NewUserFactory.get();
        user.setPrivacyAgreement("1");
        userAdapter.registerNewUser(user, 201);
        userAdapter.getCurrentUser(user, 200);
    }

    @Test(description = "Login with valid Uname and pass")
    @Feature("auth")
    @Story("Check that user can login")
    @Tags({"api", "regress"})
    @Severity(SeverityLevel.MINOR)
    public void loginTest() {
        NewUser user = NewUserFactory.get();
        user.setEmail(PropertyReader.getProperty("lucky.wins.mail"));
        user.setPassword(PropertyReader.getProperty("lucky.wins.pass"));
        userAdapter.login(user, 201);
        userAdapter.getCurrentUser(user, 200);
        userAdapter.refreshToken(201);
        userAdapter.getCurrentUser(user, 200);
        userAdapter.checkThatExpiredTokenUnavailable(500);
    }

    @Test(description = "Logout from account")
    @Feature("auth")
    @Story("Check that user can logout")
    @Tags({"api", "regress"})
    @Severity(SeverityLevel.MINOR)
    public void logoutTest() {
        NewUser user = NewUserFactory.get();
        user.setPrivacyAgreement("1");
        userAdapter.registerNewUser(user, 201);
        userAdapter.getCurrentUser(user, 200);
        userAdapter.logout(200);
        userAdapter.assertThatUserSessionHasEnded(500);
    }

    @Test(description = "Get user by id")
    @Feature("auth")
    @Story("Check that user can get account by id")
    @Tags({"api", "regress"})
    @Severity(SeverityLevel.MINOR)
    public void getUserByIdTest() {
        NewUser user = NewUserFactory.get();
        user.setEmail(PropertyReader.getProperty("lucky.wins.mail"));
        user.setPassword(PropertyReader.getProperty("lucky.wins.pass"));
        userAdapter.login(user, 201);
        userAdapter.getCurrentUser(user, 200);
        userAdapter.getUserById(200, userAdapter.getUserId());
    }
}
