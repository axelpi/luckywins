package tests;

import io.qameta.allure.*;
import org.testng.annotations.Test;
import utils.PropertyReader;

import static org.testng.Assert.assertTrue;

@Owner("alexP")
public class LoginTest extends BaseTest {

    @Test(description = "Login with valid Uname and pass")
    @Feature("auth")
    @Story("Check that user can login")
    @Tags({"web", "regress"})
    @Severity(SeverityLevel.MINOR)
    public void loginTestPositive() {
        mainPage.open();
        loginForm.fillLoginForm(PropertyReader.getProperty("lucky.wins.mail"),
                PropertyReader.getProperty("lucky.wins.pass"));
        assertTrue(mainPage.isUserLoggedIn());
    }

    @Test(description = "User can't login with invalid password")
    @Feature("auth")
    @Story("Check that user can't login with invalid password")
    @Tags({"web", "regress"})
    @Severity(SeverityLevel.MINOR)
    public void loginUsingWrongPassword() {
        mainPage.open();
        loginForm.fillLoginForm(PropertyReader.getProperty("lucky.wins.mail"),
                PropertyReader.getProperty("lucky.wins.invalid.pass"));
        assertTrue(mainPage.isUserNotLoggedIn());
    }

    @Test(description = "User can't login with invalid username")
    @Feature("auth")
    @Story("Check that user can't login with invalid Uname")
    @Tags({"web", "regress"})
    @Severity(SeverityLevel.MINOR)
    public void loginUsingWrongUsername() {
        mainPage.open();
        loginForm.fillLoginForm(PropertyReader.getProperty("lucky.wins.invalid.mail"),
                PropertyReader.getProperty("lucky.wins.pass"));
        assertTrue(mainPage.isUserNotLoggedIn());
    }

}
