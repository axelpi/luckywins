package tests;

import io.qameta.allure.Feature;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;
import models.NewUser;
import models.NewUserFactory;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;

public class RegistrationTest extends BaseTest {

    @BeforeMethod
    public void openPage() {
        mainPage.open();
    }

    @Test(description = "Register new user")
    @Feature("auth")
    @Story("Check that user can register")
    @Tags({"web", "regress"})
    @Severity(SeverityLevel.MINOR)
    public void registerTest() {
        NewUser user = NewUserFactory.get();
        mainPage.appendixRegister(user);
    }

    @Test(description = "Enter one valid char in first name field")
    @Feature("auth")
    @Story("Check that user can't enter one valid char in first name field")
    @Tags({"web", "regress"})
    @Severity(SeverityLevel.MINOR)
    public void enterOneValidCharInFirstNameTestNegative() {
        mainPage.setCustomFirstNameField("A");
    }

    @Test(description = "Enter special character in first name field")
    @Feature("auth")
    @Story("Check that user can't enter special character in first name field")
    @Tags({"web", "regress"})
    @Severity(SeverityLevel.MINOR)
    public void enterSpecialCharacterInFirstNameTestNegative() {
        mainPage.setCustomFirstNameField("first#name");
    }

    @Test(description = "Enter cyrillic letter in first name field")
    @Feature("auth")
    @Story("Check that user can't enter cyrillic letter in first name field")
    @Tags({"web", "regress"})
    @Severity(SeverityLevel.MINOR)
    public void enterCyrillicLetterInFirstNameTestNegative() {
        mainPage.setCustomFirstNameField("firstЙname");
    }

    @Test(description = "Enter value with white space in first name field")
    @Feature("auth")
    @Story("Check that user can't enter value with white space in first name field")
    @Tags({"web", "regress"})
    @Severity(SeverityLevel.MINOR)
    public void enterValueWithWhiteSpaceInFirstNameTestNegative() {
        mainPage.setCustomFirstNameField("first name");
    }

    @Test(description = "Enter value with digit in first name field")
    @Feature("auth")
    @Story("Check that user can't enter value with digit in first name field")
    @Tags({"web", "regress"})
    @Severity(SeverityLevel.MINOR)
    public void enterValueWithDigitInFirstNameTestNegative() {
        mainPage.setCustomFirstNameField("first8name");
    }

    @Test(description = "Enter empty value in first name field")
    @Feature("auth")
    @Story("Check that user can't enter empty value in first name field")
    @Tags({"web", "regress"})
    @Severity(SeverityLevel.MINOR)
    public void enterEmptyValueInFirstNameTestNegative() {
        mainPage.setCustomFirstNameField(" ");
    }

    @Test(description = "Enter invalid value in first name field")
    @Feature("auth")
    @Story("Check that user can't enter invalid value in first name field")
    @Tags({"web", "regress"})
    @Severity(SeverityLevel.MINOR)
    public void enterInvalidValueInFirstNameTestNegative() {
        mainPage.setCustomFirstNameField("First name3#й");
    }




    @Test(description = "Enter one valid char in last name field")
    @Feature("auth")
    @Story("Check that user can't enter one valid char in last name field")
    @Tags({"web", "regress"})
    @Severity(SeverityLevel.MINOR)
    public void enterOneValidCharInLastNameTestNegative() {
        mainPage.setCustomLastNameField("A");
    }

    @Test(description = "Enter special character in last name field")
    @Feature("auth")
    @Story("Check that user can't enter special character in last name field")
    @Tags({"web", "regress"})
    @Severity(SeverityLevel.MINOR)
    public void enterSpecialCharacterInLastNameTestNegative() {
        mainPage.setCustomLastNameField("first#name");
    }

    @Test(description = "Enter cyrillic letter in last name field")
    @Feature("auth")
    @Story("Check that user can't enter cyrillic letter in last name field")
    @Tags({"web", "regress"})
    @Severity(SeverityLevel.MINOR)
    public void enterCyrillicLetterInLastNameTestNegative() {
        mainPage.setCustomLastNameField("firstЙname");
    }

    @Test(description = "Enter value with white space in last name field")
    @Feature("auth")
    @Story("Check that user can't enter value with white space in last name field")
    @Tags({"web", "regress"})
    @Severity(SeverityLevel.MINOR)
    public void enterValueWithWhiteSpaceInLastNameTestNegative() {
        mainPage.setCustomLastNameField("first name");
    }

    @Test(description = "Enter value with digit in last name field")
    @Feature("auth")
    @Story("Check that user can't enter value with digit in last name field")
    @Tags({"web", "regress"})
    @Severity(SeverityLevel.MINOR)
    public void enterValueWithDigitInLastNameTestNegative() {
        mainPage.setCustomLastNameField("first8name");
    }

    @Test(description = "Enter empty value in last name field")
    @Feature("auth")
    @Story("Check that user can't enter empty value in last name field")
    @Tags({"web", "regress"})
    @Severity(SeverityLevel.MINOR)
    public void enterEmptyValueInLastNameTestNegative() {
        mainPage.setCustomLastNameField(" ");
    }

    @Test(description = "Enter invalid value in last name field")
    @Feature("auth")
    @Story("Check that user can't enter invalid value in last name field")
    @Tags({"web", "regress"})
    @Severity(SeverityLevel.MINOR)
    public void enterInvalidValueInLastNameTestNegative() {
        mainPage.setCustomLastNameField("First name3#й");
    }

    @Test(description = "Enter nine digits in phone number field")
    @Feature("auth")
    @Story("Check that user can't enter nine digits in phone number field")
    @Tags({"web", "regress"})
    @Severity(SeverityLevel.MINOR)
    public void enterNineDigitsInPhoneNumTestNegative() {
        mainPage.setCustomPhoneField("123456789");
    }

    @Test(description = "Enter sixteen digits in phone number field")
    @Feature("auth")
    @Story("Check that user can't enter sixteen digits in phone number field")
    @Tags({"web", "regress"})
    @Severity(SeverityLevel.MINOR)
    public void enterSixteenDigitsInPhoneNumTestNegative() {
        mainPage.setCustomPhoneField("1234567899876543");
    }

    @Test(description = "Enter value with latin letter in phone number field")
    @Feature("auth")
    @Story("Check that user can't enter value with latin letter in phone number field")
    @Tags({"web", "regress"})
    @Severity(SeverityLevel.MINOR)
    public void enterValueWithLatinLetterInPhoneNumTestNegative() {
        mainPage.setCustomPhoneField("1234567891w");
    }

    @Test(description = "Enter value with cyrillic letter in phone number field")
    @Feature("auth")
    @Story("Check that user can't enter value with cyrillic letter in phone number field")
    @Tags({"web", "regress"})
    @Severity(SeverityLevel.MINOR)
    public void enterValueWithCyrillicLetterInPhoneNumTestNegative() {
        mainPage.setCustomPhoneField("1234567891й");
    }

    @Test(description = "Enter special characters in phone number field")
    @Feature("auth")
    @Story("Check that user can't enter value with special characters in phone number field")
    @Tags({"web", "regress"})
    @Severity(SeverityLevel.MINOR)
    public void enterSpecialCharactersInPhoneNumTestNegative() {
        mainPage.setCustomPhoneField("1234567891@");
    }

    @Test(description = "Enter value with white space in phone number field")
    @Feature("auth")
    @Story("Check that user can't enter value with white space in phone number field")
    @Tags({"web", "regress"})
    @Severity(SeverityLevel.MINOR)
    public void enterValueWithWhiteSpaceInPhoneNumTestNegative() {
        mainPage.setCustomPhoneField("123456789 1");
    }

    @Test(description = "Enter empty input in phone number field")
    @Feature("auth")
    @Story("Check that user can't enter empty input in phone number field")
    @Tags({"web", "regress"})
    @Severity(SeverityLevel.MINOR)
    public void enterEmptyInputInPhoneNumTestNegative() {
        mainPage.setCustomPhoneField(" ");
    }

    @Test(description = "Enter invalid value in phone number field")
    @Feature("auth")
    @Story("Check that user can't enter invalid value in phone number field")
    @Tags({"web", "regress"})
    @Severity(SeverityLevel.MINOR)
    public void enterInvalidValueInPhoneNumTestNegative() {
        mainPage.setCustomPhoneField("123456789@wй");
    }

    @Test(description = "Enter value without name in email field")
    @Feature("auth")
    @Story("Check that user can't enter value without name in email field")
    @Tags({"web", "regress"})
    @Severity(SeverityLevel.MINOR)
    public void enterEmailWithoutNameTestNegative() {
        mainPage.setCustomEmailField("@test.com", false);
    }

    @Test(description = "Enter value without domain name in email field")
    @Feature("auth")
    @Story("Check that user can't enter value without domain name in email field")
    @Tags({"web", "regress"})
    @Severity(SeverityLevel.MINOR)
    public void enterEmailWithoutDomainNameTestNegative() {
        mainPage.setCustomEmailField("email@.com", false);
    }

    @Test(description = "Enter value without top-level domain in email field")
    @Feature("auth")
    @Story("Check that user can't enter value without top-level domain in email field")
    @Tags({"web", "regress"})
    @Severity(SeverityLevel.MINOR)
    public void enterEmailWithoutTopLevelDomainTestNegative() {
        mainPage.setCustomEmailField("email@test.", false);
    }

    @Test(description = "Enter value without '@' character in email field")
    @Feature("auth")
    @Story("Check that user can't enter value without '@' character in email field")
    @Tags({"web", "regress"})
    @Severity(SeverityLevel.MINOR)
    public void enterEmailWithoutAtCharacterTestNegative() {
        mainPage.setCustomEmailField("emailtest.com", false);
    }

    @Test(description = "Enter value without dot character in email field")
    @Feature("auth")
    @Story("Check that user can't enter value without dot character in email field")
    @Tags({"web", "regress"})
    @Severity(SeverityLevel.MINOR)
    public void enterEmailWithoutDotTestNegative() {
        mainPage.setCustomEmailField("email@testcom", false);
    }

    @Test(description = "Enter empty value in email field")
    @Feature("auth")
    @Story("Check that user can't enter empty value in email field")
    @Tags({"web", "regress"})
    @Severity(SeverityLevel.MINOR)
    public void enterEmptyInputInEmailTestNegative() {
        mainPage.setCustomEmailField(" ", true);
    }

    @Test(description = "Enter four valid characters in password field")
    @Feature("auth")
    @Story("Check that user can't enter four valid characters in password field")
    @Tags({"web", "regress"})
    @Severity(SeverityLevel.MINOR)
    public void enterFourValidCharactersInPasswordTestNegative() {
        mainPage.setCustomPassField("12Ww");
    }

    @Test(description = "Enter seventeen valid characters in password field")
    @Feature("auth")
    @Story("Check that user can't enter seventeen valid characters in password field")
    @Tags({"web", "regress"})
    @Severity(SeverityLevel.MINOR)
    public void enterSeventeenValidCharactersInPasswordTestNegative() {
        mainPage.setCustomPassField("123456789987654Ww");
    }

    @Test(description = "Enter value with cyrillic letter in password field")
    @Feature("auth")
    @Story("Check that user can't enter value with cyrillic letter in password field")
    @Tags({"web", "regress"})
    @Severity(SeverityLevel.MINOR)
    public void enterValueWithCyrillicInPasswordTestNegative() {
        mainPage.setCustomPassField("123456789Wwй");
    }

    @Test(description = "Enter value with special characters in password field")
    @Feature("auth")
    @Story("Check that user can't enter value with special characters in password field")
    @Tags({"web", "regress"})
    @Severity(SeverityLevel.MINOR)
    public void enterValueWithSpecialCharactersInPasswordTestNegative() {
        mainPage.setCustomPassField("123456789Ww@");
    }

    @Test(description = "Enter value with white space in password field")
    @Feature("auth")
    @Story("Check that user can't enter value with white space in password field")
    @Tags({"web", "regress"})
    @Severity(SeverityLevel.MINOR)
    public void enterValueWithWhiteSpaceInPasswordTestNegative() {
        mainPage.setCustomPassField("123456789 Ww");
    }

    @Test(description = "Enter empty input in password field")
    @Feature("auth")
    @Story("Check that user can't enter empty input in password field")
    @Tags({"web", "regress"})
    @Severity(SeverityLevel.MINOR)
    public void enterEmptyInputInPasswordTestNegative() {
        mainPage.setCustomPassField(" ");
    }

    @Test(description = "Enter value without digits in password field")
    @Feature("auth")
    @Story("Check that user can't enter value without digits in password field")
    @Tags({"web", "regress"})
    @Severity(SeverityLevel.MINOR)
    public void enterValueWithoutDigitsInPasswordTestNegative() {
        mainPage.setCustomPassField("wqwqwqwqwWw");
    }

    @Test(description = "Enter value without uppercase letters in password field")
    @Feature("auth")
    @Story("Check that user can't enter value without uppercase letters in password field")
    @Tags({"web", "regress"})
    @Severity(SeverityLevel.MINOR)
    public void enterValueWithoutUppercaseLettersInPasswordTestNegative() {
        mainPage.setCustomPassField("123456789ww");
    }

    @Test(description = "Enter value without lowercase letters in password field")
    @Feature("auth")
    @Story("Check that user can't enter value without lowercase letters in password field")
    @Tags({"web", "regress"})
    @Severity(SeverityLevel.MINOR)
    public void enterValueWithoutLowercaseLettersInPasswordTestNegative() {
        mainPage.setCustomPassField("123456789WW");
    }

    @Test(description = "Enter invalid value in password field")
    @Feature("auth")
    @Story("Check that user can't enter invalid value in password field")
    @Tags({"web", "regress"})
    @Severity(SeverityLevel.MINOR)
    public void enterInvalidValueInPasswordTestNegative() {
        mainPage.setCustomPassField("123456Ww й3@");
    }

    @Test(description = "Submit registration form without clicking agreement checkbox")
    @Feature("auth")
    @Story("Check that user will see error message when submitting without clicking agreement checkbox")
    @Tags({"web", "regress"})
    @Severity(SeverityLevel.MINOR)
    public void agreementErrorMessageTestNegative() {
        mainPage.open();
        mainPage.clickRegisterButton();
        mainPage.submitRegisterForm();
        assertTrue(mainPage.isAgreementMessageCorrect());
    }
}
