package wrappers;

import pages.BasePage;

import static com.codeborne.selenide.Selenide.$;

public class UserMenuNavigation extends BasePage {

    private static final String userMenuButtonsLocator = "[data-test-id=user-card-menu-link-%s]";

    public void navigateAndSwitchTo(String buttonName) {
        $(String.format(userMenuButtonsLocator, buttonName)).click();
        coinDisappeared();
        switchFrame();
    }
}
