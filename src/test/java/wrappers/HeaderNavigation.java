package wrappers;

import com.codeborne.selenide.Selenide;
import pages.BasePage;

import static com.codeborne.selenide.Selenide.$;

public class HeaderNavigation extends BasePage {

    private static final String navigationLocator = "[data-test-id=main-menu-link-%s]";

    public void navigateTo(String pageName) {
        Selenide.switchTo().defaultContent();
        $(String.format(navigationLocator, pageName)).click();
        coinDisappeared();
        switchFrame();
    }
}
