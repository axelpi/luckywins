package pages;

import static com.codeborne.selenide.Selenide.$x;

public class BalancePage {

    public boolean isOpened() {
        return $x("//*[contains(text(),'Main Balance')]").isDisplayed() &&
                $x("//*[contains(text(),'Casino Wallet')]").isDisplayed() &&
                $x("//*[contains(text(),'Sport Wallet')]").isDisplayed();
    }
}
