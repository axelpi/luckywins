package pages;

import io.qameta.allure.Step;

import static com.codeborne.selenide.Selenide.$x;

public class LiveCasinoPage {

    @Step("Check that live casino page is opened")
    public boolean isOpened() {
        return $x("//span[text()='Live casino']").isDisplayed();
    }
}
