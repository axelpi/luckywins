package pages;

import io.qameta.allure.Step;

import static com.codeborne.selenide.Selenide.$;

public class PromotionsPage {

    @Step("Check that promotions page is opened")
    public boolean isOpened() {
        return $(".coupon").isDisplayed();
    }
}
