package pages;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Selenide;
import io.qameta.allure.Step;
import models.NewUser;
import org.openqa.selenium.support.ui.Select;
import wrappers.HeaderNavigation;
import wrappers.LoginForm;
import wrappers.UserMenuNavigation;

import java.util.Random;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$x;
import static io.qameta.allure.Allure.step;
import static org.testng.Assert.assertTrue;

public class BasePage implements LoginForm {

    private final String loginBoxElementLocator = "[data-test-id=login-modal-%s]";
    private final String headerUsernameLocator = ".header-user-info__username";
    private final String fieldMessageLocator = "(//*[@class='v-field__message'])[%s]";

    @Override
    @Step("Fill login form and submit")
    public void fillLoginForm(String uName, String pass) {
        clickLogin();
        addUname(uName);
        addPass(pass);
        submitLoginForm();
    }

    @Step("Click login button to open login form")
    private void clickLogin() {
        Selenide.switchTo().defaultContent();
        $x("//button[text()='Login']").click();
    }

    @Step("Add username '{username}' to input")
    private void addUname(String username) {
        $(String.format(loginBoxElementLocator, "input-username")).setValue(username);
    }

    @Step("Add password '{password}' to input")
    private void addPass(String password) {
        $(String.format(loginBoxElementLocator, "input-password")).setValue(password);
    }

    @Step("Click submit button")
    private void submitLoginForm() {
        $(String.format(loginBoxElementLocator, "btn-login")).click();
        coinDisappeared();
    }

    @Deprecated
    @Step("Register new user")
    private void register(NewUser newUser) {
        clickRegisterButton();
        step("Select random currency", () -> {
            Select select = new Select($("#currencySelect"));
            select.selectByIndex(new Random().nextInt(select.getOptions().size() - 1));
        });
        step("Fill first name", () -> $("#firstname").setValue(newUser.getFirstName()));
        step("Fill last name", () -> $("#lastname").setValue(newUser.getLastName()));
        step("Fill mobile number", () -> $("#mobilephone").setValue(newUser.getMobileNum()));
        step("Fill email", () -> $("#email").setValue(newUser.getEmail()));
        step("Fill password", () -> $("#password").setValue(newUser.getPassword()));
        step("Click agreement privacy checkbox", () -> $("#user[agreement_privacy2]").click());
        step("Click 18+ checkbox", () ->
                $x("//label[contains(text(),'I`m over 18')]/ancestor::*[contains(@class,'v-field_type_checkbox')]" +
                        "//input[@type='checkbox']").click());
        submitRegisterForm();
        assertTrue(isUserLoggedInAfterReg(newUser.getFirstName()));
    }

    @Step("Register new user")
    public void appendixRegister(NewUser newUser) {
        clickRegisterButton();
        step("Select random currency", () -> {
            Select select = new Select($("select"));
            select.selectByIndex(newUser.getCurrency());
        });
        step("Fill first name", () -> $x("(//input[@type='text'])[1]").setValue(newUser.getFirstName()));
        step("Fill last name", () -> $x("(//input[@type='text'])[2]").setValue(newUser.getLastName()));
        step("Fill mobile number", () -> $x("(//input[@type='text'])[3]").setValue(newUser.getMobileNum()));
        step("Fill email", () -> $x("//input[@type='email']").setValue(newUser.getEmail()));
        step("Fill password", () -> $x("//input[@type='password']").setValue(newUser.getPassword()));
        step("Click agreement privacy checkbox", () -> $x("(//input[@type='checkbox'])[1]").click());
        submitRegisterForm();
        assertTrue(isUserLoggedInAfterReg(newUser.getFirstName()));
    }

    @Deprecated
    @Step("Try register with invalid data")
    public void negativeAppendixRegister() {
        clickRegisterButton();

        step("Fill first name", () -> {
            $x("(//input[@type='text'])[1]").setValue("A");
            submitRegisterForm();
            assertTrue(isFirstNameMessageCorrect());
            $x("(//input[@type='text'])[1]").setValue("first#name");
            assertTrue(isFirstNameMessageCorrect());
            $x("(//input[@type='text'])[1]").setValue("firstЙname");
            assertTrue(isFirstNameMessageCorrect());
            $x("(//input[@type='text'])[1]").setValue("first name");
            assertTrue(isFirstNameMessageCorrect());
            $x("(//input[@type='text'])[1]").setValue("first8name");
            assertTrue(isFirstNameMessageCorrect());
            $x("(//input[@type='text'])[1]").setValue(" ");
            assertTrue(isFirstNameMessageCorrect());
            $x("(//input[@type='text'])[1]").setValue("First name3#й");
            assertTrue(isFirstNameMessageCorrect());
        });

        step("Fill last name", () -> {
            $x("(//input[@type='text'])[2]").setValue("A");
            assertTrue(isLastNameMessageCorrect());
            $x("(//input[@type='text'])[2]").setValue("last#name");
            assertTrue(isLastNameMessageCorrect());
            $x("(//input[@type='text'])[2]").setValue("lastЙname");
            assertTrue(isLastNameMessageCorrect());
            $x("(//input[@type='text'])[2]").setValue("last name");
            assertTrue(isLastNameMessageCorrect());
            $x("(//input[@type='text'])[2]").setValue("last8name");
            assertTrue(isLastNameMessageCorrect());
            $x("(//input[@type='text'])[2]").setValue(" ");
            assertTrue(isLastNameMessageCorrect());
            $x("(//input[@type='text'])[2]").setValue("last name3#й");
            assertTrue(isLastNameMessageCorrect());
        });

        step("Fill mobile number", () -> {
            $x("(//input[@type='text'])[3]").setValue("123456789");
            assertTrue(isPhoneMessageCorrect());
            $x("(//input[@type='text'])[3]").setValue("1234567899876543");
            assertTrue(isPhoneMessageCorrect());
            $x("(//input[@type='text'])[3]").setValue("1234567891w");
            assertTrue(isPhoneMessageCorrect());
            $x("(//input[@type='text'])[3]").setValue("1234567891й");
            assertTrue(isPhoneMessageCorrect());
            $x("(//input[@type='text'])[3]").setValue("1234567891@");
            assertTrue(isPhoneMessageCorrect());
            $x("(//input[@type='text'])[3]").setValue("123456789 1");
            assertTrue(isPhoneMessageCorrect());
            $x("(//input[@type='text'])[3]").setValue(" ");
            assertTrue(isPhoneMessageCorrect());
            $x("(//input[@type='text'])[3]").setValue("123456789@wй");
            assertTrue(isPhoneMessageCorrect());
        });

        step("Fill email", () -> {
            $x("//input[@type='email']").setValue("@test.com");
            assertTrue(isEmailMessageCorrect(false));
            $x("//input[@type='email']").setValue("email@.com");
            assertTrue(isEmailMessageCorrect(false));
            $x("//input[@type='email']").setValue("email@test.");
            assertTrue(isEmailMessageCorrect(false));
            $x("//input[@type='email']").setValue("emailtest.com");
            assertTrue(isEmailMessageCorrect(false));
            $x("//input[@type='email']").setValue("email@testcom");
            assertTrue(isEmailMessageCorrect(false));
            $x("//input[@type='email']").setValue(" ");
            assertTrue(isEmailMessageCorrect(true));
        });

        step("Fill password", () -> {
            $x("//input[@type='password']").setValue("12Ww");
            assertTrue(isPasswordMessageCorrect());
            $x("//input[@type='password']").setValue("123456789987654Ww");
            assertTrue(isPasswordMessageCorrect());
            $x("//input[@type='password']").setValue("123456789Wwй");
            assertTrue(isPasswordMessageCorrect());
            $x("//input[@type='password']").setValue("123456789Ww@");
            assertTrue(isPasswordMessageCorrect());
            $x("//input[@type='password']").setValue("123456789 Ww");
            assertTrue(isPasswordMessageCorrect());
            $x("//input[@type='password']").setValue(" ");
            assertTrue(isPasswordMessageCorrect());
            $x("//input[@type='password']").setValue("wqwqwqwqwWw");
            assertTrue(isPasswordMessageCorrect());
            $x("//input[@type='password']").setValue("123456789ww");
            assertTrue(isPasswordMessageCorrect());
            $x("//input[@type='password']").setValue("123456789WW");
            assertTrue(isPasswordMessageCorrect());
            $x("//input[@type='password']").setValue("123456Ww й3@");
            assertTrue(isPasswordMessageCorrect());
        });

        step("Don't click agreement privacy checkbox", () -> assertTrue(isAgreementMessageCorrect()));
    }

    @Step("Set '{firstName}' in first name field")
    public void setCustomFirstNameField(String firstName) {
        clickRegisterButton();
        $x("(//input[@type='text'])[1]").setValue(firstName);
        submitRegisterForm();
        assertTrue(isFirstNameMessageCorrect());
    }

    @Step("Set '{lastName}' in last name field")
    public void setCustomLastNameField(String lastName) {
        clickRegisterButton();
        $x("(//input[@type='text'])[2]").setValue(lastName);
        submitRegisterForm();
        assertTrue(isLastNameMessageCorrect());
    }

    @Step("Set '{phoneNum}' in phone number field")
    public void setCustomPhoneField(String phoneNum) {
        clickRegisterButton();
        $x("(//input[@type='text'])[3]").setValue(phoneNum);
        submitRegisterForm();
        assertTrue(isPhoneMessageCorrect());
    }

    @Step("Set '{email}' in email field")
    public void setCustomEmailField(String email, boolean isEmpty) {
        clickRegisterButton();
        $x("//input[@type='email']").setValue(email);
        submitRegisterForm();
        assertTrue(isEmailMessageCorrect(isEmpty));
    }

    @Step("Set '{pass}' in password field")
    public void setCustomPassField(String pass) {
        clickRegisterButton();
        $x("//input[@type='password']").setValue(pass);
        submitRegisterForm();
        assertTrue(isPasswordMessageCorrect());
    }

    @Step("Click register button")
    public void clickRegisterButton() {
        Selenide.switchTo().defaultContent();
        $("[data-test-id=header-btn-show-register-modal]").click();
    }

    @Step("Submit register form")
    public void submitRegisterForm() {
        Selenide.sleep(200);
        $(String.format(loginBoxElementLocator, "btn-login")).click();
    }

    @Step("Check that first name error message is correct")
    public boolean isFirstNameMessageCorrect() {
        return $x(String.format(fieldMessageLocator, 1)).getText()
                .equals("First Name. Required. Only letters allowed.");
    }

    @Step("Check that last name error message is correct")
    public boolean isLastNameMessageCorrect() {
        return $x(String.format(fieldMessageLocator, 2)).getText()
                .equals("Last Name. Required. Only letters allowed.");
    }

    @Step("Check that phone number error message is correct")
    public boolean isPhoneMessageCorrect() {
        return $x(String.format(fieldMessageLocator, 3)).getText()
                .equals("Phone Error number.(Ex. 123456789 )");
    }

    @Step("Check that email error message is correct")
    public boolean isEmailMessageCorrect(boolean isEmailFieldEmpty) {
        if (isEmailFieldEmpty)
            return $x(String.format(fieldMessageLocator, 4)).getText()
                    .equals("Please enter your email");
        else
            return $x(String.format(fieldMessageLocator, 4)).getText()
                    .equals("Not valid email. (Ex. name@domain.com)");
    }

    @Step("Check that password error message is correct")
    public boolean isPasswordMessageCorrect() {
        return $x(String.format(fieldMessageLocator, 5)).getText()
                .equals("Password.(6-16) At least: 1-digit,1-upp,1-low");
    }

    @Step("Check that agreement policy error message is correct")
    public boolean isAgreementMessageCorrect() {
        return $x(String.format(fieldMessageLocator, 6)).getText()
                .equals("You have to agree with Privacy and Policy.");
    }

    @Step("Click main page logo and switch to it")
    public void openMainPage() {
        Selenide.switchTo().defaultContent();
        $("[data-test-id=header-logo-link]").click();
        coinDisappeared();
        switchFrame();
    }

    @Step("Click promotions page and switch to it")
    public void openPromotions() {
        new HeaderNavigation().navigateTo("promotions");
        assertTrue(new PromotionsPage().isOpened());
    }

    @Step("Click casino games page and switch to it")
    public void openCasinoGamesPage() {
        new HeaderNavigation().navigateTo("casino-games");
        assertTrue(new CasinoGamesPage().isOpened());
    }

    @Step("Click live casino page and switch to it")
    public void openLiveCasinoPage() {
        new HeaderNavigation().navigateTo("live-casino");
        assertTrue(new LiveCasinoPage().isOpened());
    }

    @Step("Click sport betting page and switch to it")
    public void openSportBettingPage() {
        new HeaderNavigation().navigateTo("sport-betting");
        Selenide.switchTo().frame($("#gameIFrame"));
        assertTrue(new SportBettingPage().isOpened());
    }

    @Step("Click user button to open user info menu")
    private static void openUserMenu() {
        Selenide.switchTo().defaultContent();
        $("[data-test-id=header-btn-show-user-info-popover]").click();
    }

    @Step("Open user balance")
    public void openUserBalance() {
        openUserMenu();
        new UserMenuNavigation().navigateAndSwitchTo("casino-balance");
        assertTrue(new BalancePage().isOpened());
    }

    @Step("Open available coupons")
    public void openAvailableCoupons() {
        openUserMenu();
        new UserMenuNavigation().navigateAndSwitchTo("available-coupon");
        assertTrue(new MyCouponsPage().isOpened());
    }

    @Step("Open account details")
    public void openAccountDetails() {
        openUserMenu();
        new UserMenuNavigation().navigateAndSwitchTo("account-details");
        assertTrue(new AccountDetailsPage().isOpened());
    }

    @Step("Sign out")
    public void signOut() {
        openUserMenu();
        $x("//div[contains(text(),'Sign Out')]").click();
        coinDisappeared();
        assertTrue(isUserNotLoggedIn());
    }

    @Step("Check that user is completely logged in")
    public boolean isUserLoggedIn() {
        Selenide.switchTo().defaultContent();
        $(headerUsernameLocator).shouldBe(Condition.visible);
        return $(headerUsernameLocator).isDisplayed();
    }

    @Step("Check that user not logged in")
    public boolean isUserNotLoggedIn() {
        Selenide.switchTo().defaultContent();
        $(headerUsernameLocator).shouldBe(Condition.visible);
        return !$(headerUsernameLocator).isDisplayed();
    }

    @Step("Check that user '{firstname}' completely logged in after registration")
    private boolean isUserLoggedInAfterReg(String firstname) {
        $(headerUsernameLocator).shouldBe(Condition.visible);
        return $(headerUsernameLocator).getText().equals(firstname);
    }

    @Step("Loading coin disappeared")
    protected void coinDisappeared() {
        $(".lds-circle").shouldBe(Condition.disappear);
    }

    @Step("Switch to current frame")
    protected void switchFrame() {
        Selenide.switchTo().innerFrame("iframe-instance");
    }

}
