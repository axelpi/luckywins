package pages;

import com.codeborne.selenide.Selenide;
import io.qameta.allure.Step;
import org.openqa.selenium.support.ui.Select;

import java.util.List;
import java.util.Objects;
import java.util.Random;
import java.util.stream.Collectors;

import static com.codeborne.selenide.Selenide.*;
import static org.testng.Assert.assertTrue;

public class CasinoGamesPage extends BasePage {

    @Step("Check that casino games page is opened")
    public boolean isOpened() {
        return $("#games_cont").isDisplayed();
    }

    @Step("Open casino games page via url")
    public void open() {
        Selenide.open("games");
        switchFrame();
    }

    @Step("Find games with word '{name}'")
    public void searchGame(String name) {
        $("#name_filter").sendKeys(name);
        assertTrue(isGamesFoundCorrect(name));
    }

    @Step("Select random type of game")
    public void selectRandomTypeOfGame() {
        Select select = new Select($("#type-select"));
        int randomTypeIndex = new Random().nextInt(select.getOptions().size() - 1);
        String randomType = select.getOptions().get(randomTypeIndex).getText();
        select.selectByIndex(randomTypeIndex);
        assertTrue(new CasinoGamesPage().isDisplayedTypeCorrect(randomType));
    }

    @Step("Select random game provider")
    public void selectRandomProvider() {
        Select select = new Select($("#man-select"));
        int randomProviderIndex = new Random().nextInt(select.getOptions().size() - 1);
        String randomProvider = select.getOptions().get(randomProviderIndex).getText();
        select.selectByIndex(randomProviderIndex);
        assertTrue(new CasinoGamesPage().isDisplayedProviderCorrect(randomProvider));
    }

    @Step("Check that all filtered games have word '{name}' in their name")
    private boolean isGamesFoundCorrect(String name) {
        Selenide.sleep(1500);
        List<String> gameNames = $$x("//div[@id='games_cont']//div[contains(@style,'block')]")
                .stream().map(s -> Objects.requireNonNull(s.getAttribute("data-name")).toLowerCase())
                .collect(Collectors.toList());
        boolean isDataCorrect;
        isDataCorrect = !gameNames.isEmpty();
        for (String gameName : gameNames) {
            if (!gameName.contains(name)) {
                isDataCorrect = false;
                break;
            }
        }
        return isDataCorrect;
    }

    @Step("Check that type '{type}' displayed correct")
    private boolean isDisplayedTypeCorrect(String type) {
        return $x(String.format("//*[@id='switch-title'][text()='%s']", type)).isDisplayed();
    }

    @Step("Check that provider '{provider}' displayed correct")
    private boolean isDisplayedProviderCorrect(String provider) {
        return $("#switch-tag").getText().contains(provider);
    }
}
