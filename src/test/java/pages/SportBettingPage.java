package pages;

import io.qameta.allure.Step;

import static com.codeborne.selenide.Selenide.$x;

public class SportBettingPage {

    @Step("Check that sport betting page is opened")
    public boolean isOpened() {
        return $x("//a[contains(text(),'Live Betting')]").isDisplayed();
    }
}
