package pages;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Selenide;
import io.qameta.allure.Step;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;
import static org.testng.Assert.assertTrue;

public class MainPage extends BasePage {

    private final String activePromotionImgLocator = ".main-promotion.active .main-promotion-image";
    private final String imgAttribute = "src";

    @Step("Check that main page is opened")
    public boolean isOpened() {
        return $(".main-promotion-container").isDisplayed();
    }

    @Step("Open main page via url")
    public void open() {
        Selenide.open("");
    }

    @Step("Spin main promotion to {direction}")
    public void spinMainPromotion(String direction) {
        switchFrame();
        List<String> promotionImgs = $$(".main-promotion-image").stream()
                .map(selenideElement -> selenideElement.getAttribute(imgAttribute)).collect(Collectors.toList());
        String oldActiveImg = $(activePromotionImgLocator).getAttribute(imgAttribute);
        int oldActiveImgId = -1;
        for (int i = 0; i <= promotionImgs.size() - 1; i++) {
            if (promotionImgs.get(i).equals(oldActiveImg)) {
                oldActiveImgId = i;
                break;
            }
        }
        $(String.format(".carousel-control.%s", direction.toLowerCase())).click();
        switch (direction.toLowerCase()) {
            case "right":
                $(".carousel-item-left").shouldBe(Condition.disappear);
                break;
            case "left":
                $(".carousel-item-right").shouldBe(Condition.disappear);
                break;
        }
        assertTrue(new MainPage().isActivePromotionChangedCorrectly(oldActiveImgId, promotionImgs, direction.toLowerCase()));
    }

    @Step("Check that promotion changed correctly")
    private boolean isActivePromotionChangedCorrectly(int oldActiveImgId, List<String> promotionImgs, String direction) {
        switch (direction) {
            case "right": {
                if (oldActiveImgId == promotionImgs.size() - 1) {
                    return Objects.equals($(activePromotionImgLocator).getAttribute(imgAttribute),
                            promotionImgs.get(0));
                } else {
                    return Objects.equals($(activePromotionImgLocator).getAttribute(imgAttribute),
                            promotionImgs.get(oldActiveImgId + 1));
                }
            }
            case "left": {
                if (oldActiveImgId == 0) {
                    return Objects.equals($(activePromotionImgLocator).getAttribute(imgAttribute),
                            promotionImgs.get(promotionImgs.size() - 1));
                } else {
                    return Objects.equals($(activePromotionImgLocator).getAttribute(imgAttribute),
                            promotionImgs.get(oldActiveImgId - 1));
                }
            }
        }
        return false;
    }
}
