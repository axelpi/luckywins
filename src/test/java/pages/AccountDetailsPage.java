package pages;

import static com.codeborne.selenide.Selenide.$x;

public class AccountDetailsPage {

    public boolean isOpened() {
        return $x("//*[contains(text(),'Full Name')]").isDisplayed();
    }
}
