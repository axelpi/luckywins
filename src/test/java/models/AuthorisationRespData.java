package models;

import com.google.gson.annotations.SerializedName;
import lombok.Data;

@Data
public class AuthorisationRespData {

    @SerializedName("token_type")
    private String tokenType;
    @SerializedName("expires_in")
    private String expiresIn;
    @SerializedName("access_token")
    private String accessToken;
    @SerializedName("refresh_token")
    private String refreshToken;

}
