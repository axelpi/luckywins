package models;

import com.google.gson.annotations.SerializedName;
import lombok.Data;

@Data
public class LoginRequest {
    @SerializedName("username")
    String email;
    String password;

}
