package models;

import com.github.javafaker.Faker;

import java.util.Random;

public class NewUserFactory {

    public static NewUser get() {
        NewUser newUser = new NewUser();
        Faker faker = new Faker();
        newUser.setCurrency(1);
        newUser.setFirstName(faker.name().firstName());
        newUser.setLastName(faker.name().lastName());
        newUser.setMobileNum(getMobileNum());
        newUser.setEmail(faker.internet().emailAddress());
        newUser.setPassword("qwertyI1");
        return newUser;
    }

    private static String getMobileNum() {
        Random random = new Random();
        StringBuilder mobileNum = new StringBuilder();
        for (int i = 0; i <10; i++) {
            mobileNum.append(random.nextInt(9));
        }
        return mobileNum.toString();
    }
}
