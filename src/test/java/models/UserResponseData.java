package models;

import lombok.Data;

@Data
public class UserResponseData {
    private String uid;
    private String username;
}
