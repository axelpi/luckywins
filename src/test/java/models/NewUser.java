package models;

import com.google.gson.annotations.SerializedName;
import lombok.Data;

@Data
public class NewUser {

    @SerializedName("currency_id")
    int currency;
    @SerializedName("first_name")
    String firstName;
    @SerializedName("last_name")
    String lastName;
    @SerializedName("cellphone_number")
    String mobileNum;
    @SerializedName("username")
    String email;
    String password;
    @SerializedName("privacy_agreement")
    String privacyAgreement;

}
