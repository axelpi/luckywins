package adapters;

import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import io.qameta.allure.Step;
import io.restassured.http.Cookie;
import models.*;

import static org.testng.Assert.assertEquals;

public class UserAdapter extends BaseAdapter {

    private String authToken;
    private String refreshToken;
    private String userId;
    private String expiredToken;

    @Step("Register new user")
    public void registerNewUser(NewUser newUser, int statusCode) {
        String body = post(new Gson().toJson(newUser), "auth/register", "", statusCode);
        Response<AuthorisationRespData> response =
                new Gson().fromJson(body, new TypeToken<Response<AuthorisationRespData>>() {
                }.getType());
        assertEquals(response.getStatus(), "Success");
        expiredToken = authToken;
        authToken = response.getData().getAccessToken();
        refreshToken = response.getData().getRefreshToken();
    }

    @Step("Get current user")
    public void getCurrentUser(NewUser user, int statusCode) {
        String body = get("users/current", authToken, statusCode);
        Response<UserResponseData> response =
                new Gson().fromJson(body, new TypeToken<Response<UserResponseData>>() {
                }.getType());
        userId = response.getData().getUid();
        assertEquals(response.getStatus(), "Success");
        assertEquals(response.getData().getUsername(), user.getEmail());
    }

    @Step("Check that expired token unavailable")
    public void checkThatExpiredTokenUnavailable(int statusCode) {
        get("users/current", expiredToken, statusCode);
    }

    @Step("Check that user session has ended")
    public void assertThatUserSessionHasEnded(int statusCode) {
        get("users/current", authToken, statusCode);
    }

    @Step("Logout")
    public void logout(int statusCode) {
        String body = post("", "auth/logout", authToken, statusCode);
        Response response = new Gson().fromJson(body, Response.class);
        assertEquals(response.getStatus(), "Success");
    }

    @Step("Login using credentials")
    public void login(NewUser user, int statusCode) {
        LoginRequest loginRequest = new LoginRequest();
        loginRequest.setEmail(user.getEmail());
        loginRequest.setPassword(user.getPassword());
        String body = post(new Gson().toJson(loginRequest), "auth/login", "", statusCode);
        Response<AuthorisationRespData> response =
                new Gson().fromJson(body, new TypeToken<Response<AuthorisationRespData>>() {
                }.getType());
        assertEquals(response.getStatus(), "Success");
        expiredToken = authToken;
        authToken = response.getData().getAccessToken();
        refreshToken = response.getData().getRefreshToken();
    }

    @Step("Refresh token")
    public void refreshToken(int statusCode) {
        Cookie cookie = new Cookie.Builder("token", refreshToken)
                .build();
        String body = post("", "auth/refresh", "", statusCode, cookie);
        Response<AuthorisationRespData> response =
                new Gson().fromJson(body, new TypeToken<Response<AuthorisationRespData>>() {
                }.getType());
        assertEquals(response.getStatus(), "Success");
        expiredToken = authToken;
        authToken = response.getData().getAccessToken();
        refreshToken = response.getData().getRefreshToken();
    }

    @Step("Get user by id '{userId}'")
    public void getUserById(int statusCode, String userId) {
        String body = get("users/" + userId, "", statusCode);
        Response<UserResponseData> response =
                new Gson().fromJson(body, new TypeToken<Response<UserResponseData>>() {
                }.getType());
        assertEquals(response.getStatus(), "Success");
        assertEquals(response.getData().getUid(), userId);
    }

    public String getUserId() {
        return userId;
    }

}
