package adapters;

import io.qameta.allure.Step;
import io.restassured.RestAssured;
import io.restassured.config.SSLConfig;
import io.restassured.http.Cookie;
import utils.PropertyReader;

import static io.restassured.RestAssured.given;

public class BaseAdapter{

    final String BASE_URL = PropertyReader.getProperty("lucky.wins.api.url");
    final SSLConfig config = new SSLConfig().relaxedHTTPSValidation();

    @Step("POST request")
    String post(String body, String uri, String authToken, int statusCode) {
        RestAssured.config = RestAssured.config().sslConfig(config);
        return given()
                .header("Content-Type", "application/json")
                .auth().oauth2(authToken)
                .body(body)
                .when()
                .post(BASE_URL + uri)
                .then()
                .log().all()
                .statusCode(statusCode)
                .extract().body().asString();
    }

    @Step("POST request with cookie")
    String post(String body, String uri, String authToken, int statusCode, Cookie cookie) {
        RestAssured.config = RestAssured.config().sslConfig(config);
        return given()
                .cookie(cookie)
                .header("Content-Type", "application/json")
                .auth().oauth2(authToken)
                .body(body)
        .when()
                .post(BASE_URL + uri)
        .then()
                .log().all()
                .statusCode(statusCode)
                .extract().body().asString();
    }

    @Step("GET request")
    String get(String uri, String authToken, int statusCode) {
        RestAssured.config = RestAssured.config().sslConfig(config);
        return given()
                .header("Content-Type", "application/json")
                .auth().oauth2(authToken)
        .when()
                .get(BASE_URL + uri)
        .then()
                .log().all()
                .statusCode(statusCode)
                .extract().body().asString();
    }
}
